-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 23, 2017 at 07:04 م
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smrguidewallpaper`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `category_image` varchar(255) CHARACTER SET utf8 NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `category_name`, `category_image`, `menu_id`) VALUES
(38, 'Getting started', '6162-2017-01-23.png', 0),
(39, 'Basic moves', '1040-2017-01-23.png', 0),
(40, 'Challenge coins', '7275-2017-01-23.png', 0),
(41, 'Action objects', '0467-2017-01-23.jpg', 0),
(42, 'Normal jump', '6537-2017-01-23.gif', 1),
(43, 'High jump', '5194-2017-01-23.gif', 1),
(44, 'Spin jump', '0565-2017-01-23.gif', 1),
(45, 'Flip jump', '2379-2017-01-23.gif', 1),
(46, 'Wall reverse jump', '8981-2017-01-23.gif', 1),
(47, 'Hit multiple blocks', '8411-2017-01-23.gif', 1),
(48, 'A mid-air stall', '2185-2017-01-23.gif', 1),
(49, 'Spin jump to grab the flag', '5468-2017-01-23.gif', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `image_date` date NOT NULL,
  `image` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `cat_id`, `image_date`, `image`) VALUES
(30, 38, '0000-00-00', '2872-2017-01-23.png'),
(33, 39, '0000-00-00', '1618-2017-01-23.png'),
(34, 39, '0000-00-00', '9035-2017-01-23.png'),
(35, 39, '0000-00-00', '3194-2017-01-23.png'),
(36, 40, '0000-00-00', '1922-2017-01-23.png'),
(37, 40, '0000-00-00', '7198-2017-01-23.png'),
(38, 40, '0000-00-00', '9426-2017-01-23.png'),
(39, 41, '0000-00-00', '7770-2017-01-23.png'),
(40, 41, '0000-00-00', '9587-2017-01-23.png'),
(41, 41, '0000-00-00', '7763-2017-01-23.png'),
(42, 41, '0000-00-00', '2018-2017-01-23.png'),
(43, 41, '0000-00-00', '1239-2017-01-23.png'),
(44, 41, '0000-00-00', '5688-2017-01-23.png'),
(45, 41, '0000-00-00', '2830-2017-01-23.png'),
(46, 41, '0000-00-00', '4066-2017-01-23.png'),
(47, 42, '0000-00-00', '4241-2017-01-23.gif'),
(48, 43, '0000-00-00', '5889-2017-01-23.gif'),
(49, 44, '0000-00-00', '8873-2017-01-23.gif'),
(50, 45, '0000-00-00', '1826-2017-01-23.gif'),
(51, 46, '0000-00-00', '2213-2017-01-23.gif'),
(52, 47, '0000-00-00', '6042-2017-01-23.gif'),
(53, 48, '0000-00-00', '6967-2017-01-23.gif'),
(54, 49, '0000-00-00', '5179-2017-01-23.gif');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `ID` int(11) NOT NULL,
  `Username` varchar(15) NOT NULL,
  `Password` text NOT NULL,
  `Email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`ID`, `Username`, `Password`, `Email`) VALUES
(1, 'admin', 'd82494f05d6917ba02f7aaa29689ccb444bb73f20380876cb05d1f37537b7892', 'developer.solodroid@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
